# **JavaScript Design Patterns**
![Image](https://drive.google.com/uc?export=view&id=1lh5IWp7jArVF0JAuPOYnBshfUpeYSaD6 "Logo") 
___
<p> In this Article, I'm going to talk about design patterns that can be and should be used to write better, maintable JavaScript code.<p>

## **Introduction**
---
<p> We write code to solve problems. These problem usually have many similarities, and when trying to solve them, we notice several common patterns. This is where design patterns come in. <p>

>A **Design Pattern** is a term used in software enginnering for a general, reusable solution to a commonly occurring problem in software design. 

<p> Design patterns are beneficial for various reasons. They are proven solutions that industry veterans have tried and tested. They are solid approaches that solve issues in a widely accepted way and reflect the experience and insights of the industry-leading developers that helped define them. Patterns also make your code more reusable and readable while speeding up the development process vastly.
<p>Design patterns are by no means finished solutions. They only provide us with approaches or schemes to solve a problem.<p>
<br>

## **Categories Of Design Patterns**
___
Design patterns are usually categorized into __three__ major groups.

### **Creational Design Patterns**
<p> As the name suggests, these patterns are for handling object creational mechanisms. A creational design pattern basically solves a problem by controlling the creation process of an object.<p>

### **Structural Design Patterns**

<p>These patterns are concerned with class and object composition. They help structure or restructure one or more parts without affecting the entire system. In other words, they help obtain new functionalities without tampering with the existing ones.<p>

### **Behavioral Design Patterns**
<p>These patterns are concerned with improving communication between dissimilar objects.<p>
<br>

## **Factory Pattern**
___
[ **Factory Pattern** ]( https://www.youtube.com/watch?v=kuirGzhGhyw&list=LL&index=1&t=118s "Factory Pattern")  is another class-based creational pattern. In this, we provide a generic interface that delegates the responsibility of object instantiation to its subclasses.
<p>This pattern is frequently used when we need to manage or manipulate collections of objects that are different yet have many similar characteristics.<p>

``` javascript
    function Developer(name)
    {
        this.name = name
        this.type = "Developer"
    }

    function Tester(name)
    {
        this.name = name
        this.type = "Tester"
    }

    function EmployeeFactory()
    {
            this.create = (name, type) => {
            switch(type)
            {
                case 1:
                    return new Developer(name)
                case 2:
                    return new Tester(name)
            }
        }
    }

    function say()
    {
        console.log("Hi, I am " + this.name + " and I am a " + this.type)
    }

    const employeeFactory = new EmployeeFactory()
    const employees = []

    employees.push(employeeFactory.create("Patrick", 1))
    employees.push(employeeFactory.create("John", 2))
    employees.push(employeeFactory.create("Jamie", 1))
    employees.push(employeeFactory.create("Taylor", 1))
    employees.push(employeeFactory.create("Tim", 2))

    employees.forEach( emp => {
    say.call(emp)
    })
```
<br/>

## **Singleton Pattern**
___
[__Singleton Pattern__](https://www.youtube.com/watch?v=JKNjfDCNPa4&t=38s "Singleton Pattern") is a special creational design pattern in which only one instance of a class can exist. It works like this — if no instance of the singleton class exists then a new instance is created and returned, but if an instance already exists, then the reference to the existing instance is returned.
<p>A perfect real-life example would be that of mongoose (the famous Node.js ODM library for MongoDB). It utilizes the singleton pattern.<p>

``` javascript
    const Singleton = (function(){
    let pManager

    function ProcessManager() { /*...*/ }

    function createProcessManager()
    {
        pManager = new ProcessManager()
        return pManager
    }

    return {
        getProcessManager: () =>
        {
            if(!pManager)
            pManager = createProcessManager()
            return pManager
        }
    }
    })()

    const singleton = Singleton.getProcessManager()
    const singleton2 = Singleton.getProcessManager()

    console.log(singleton === singleton2) // true
```
<br/>

## [**Strategy Pattern** ](https://www.youtube.com/watch?v=SicL4fYCz8w&t=332s "Strategy Pattern")
___
<p>It is a behavioural design pattern that allows encapsulation of alternative algorithms for a particular task. It defines a family of algorithms and encapsulates them in such a way that they are interchangeable at runtime without client interference or knowledge.<p>

``` javascript
    function Fedex(pkg)
    {
        this.calculate = () =>
        {
            // Fedex calculations ...
            return 2.45
        }
    }

    function UPS(pkg)
    {
        this.calculate = () =>
        {
            // UPS calculations ...
            return 1.56
        }
    }

    function USPS(pkg)
    {
        this.calculate = () =>
        {
            // USPS calculations ...
            return 4.5
        }
    }

    function Shipping()
    {
        this.company = ''
        this.setStrategy = company =>
        {
            this.company = company
        }
        this.calculate = pkg => {
            return this.company.calculate(pkg)
        }
    }

    const fedex = new Fedex()
    const ups = new UPS()
    const usps = new USPS()
    const shipping = new Shipping()
    const pkg = { from: "Alabama", to: "Georgia", weight: 1.56 } // Dummy package

    shipping.setStrategy(fedex)
    console.log("Fedex: " + shipping.calculate(pkg))

    shipping.setStrategy(ups)
    console.log("UPS: " + shipping.calculate(pkg))

    shipping.setStrategy(usps)
    console.log("USPS: " + shipping.calculate(pkg))
```
<br/>

## [**Iterator Pattern**](https://www.youtube.com/watch?v=c85EStPZR8M "Iterator Pattern")
___
<p>It is a behavioural design pattern that provides a way to access the elements of an aggregate object sequentially without exposing its underlying representation.<p>
<p>Iterators have a special kind of behaviour where we step through an ordered set of values one at a time by calling next() until we reach the end. The introduction of Iterator and Generators in ES6 made the implementation of the iterator pattern extremely straightforward.<p>

```javascript
    const items = [1, false, "Devsage", 3.14]

    function Iterator(items)
    {
        this.items = items
        this.index = 0
    }

    Iterator.prototype = {
        hasNext: function()
        {
            return this.index < this.items.length
        },
        next: function()
        {
            return this.items[this.index++]
        }
    }

    const iter = new Iterator(items)

    console.log(iter.hasNext())

    while(iter.hasNext())
    console.log(iter.next())

    console.log(iter.hasNext())
```
<br/>

## **Proxy Pattern**
___
<p>This is a structural design pattern that behaves exactly as its name suggests. It acts as a surrogate or placeholder for another object to control access to it.<p>
<p>It is usually used in situations in which a target object is under constraints and may not be able to handle all its responsibilities efficiently. A proxy, in this case, usually provides the same interface to the client and adds a level of indirection to support controlled access to the target object to avoid undue pressure on it.<p>

The [**Proxy Pattern**](https://www.youtube.com/watch?v=SFTpSFQNPts&t=287s "Proxy Pattern") can be very useful when working with network request-heavy applications to avoid unnecessary or redundant network requests.

``` javascript
    function networkFetch(url) {
        return `${url} - Response from network`;
    }

    // Proxy
    // ES6 Proxy API = new Proxy(target, handler);
    const cache = [];
    const proxiedNetworkFetch = new Proxy(networkFetch, {
    apply(target, thisArg, args) {
            const urlParam = args[0];
            if (cache.includes(urlParam)) {
                return `${urlParam} - Response from cache`;
            } else {
                cache.push(urlParam);
                return Reflect.apply(target, thisArg, args);
            }
        },
    });

    // usage
    console.log(proxiedNetworkFetch('dogPic.jpg')); // 'dogPic.jpg - Response from   network'
    console.log(proxiedNetworkFetch('dogPic.jpg')); // 'dogPic.jpg - Response from cache'
```
<br/>

## **Conclusion**
___
<p>Design patterns are crucial to software engineering and can be very helpful in solving common problems. But this is a very vast subject, and it is simply not possible to include everything about them in a short piece. Therefore, I made the choice to shortly and concisely talk only about the ones I think can be really handy in writing modern JavaScript. To dive deeper, I suggest you take a look at these books:<p>

1. [ Design Patterns: Elements Of Reusable Object-Oriented Software ](https://en.wikipedia.org/wiki/Design_Patterns " Design Patterns")by Erich Gamma, Richard Helm, Ralph Johnson and John Vlissides (Gang of Four).<br/>

2. [Learn JavaScript Design Patterns ](https://addyosmani.com/resources/essentialjsdesignpatterns/book/ " Learn JavaScript Design Pattern") by Addy Osmani.

3. [JavaScript Patterns](https://www.amazon.com/JavaScript-Patterns-Stoyan-Stefanov/dp/0596806752 "Amazon.com") by Stoyan Stefanov.

